﻿using System;
using System.Linq;

namespace WeatherSorter {
    public class WeatherDatum {
        private readonly string[] _months = new string[] {
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            };

        private static int _count = 0;
        public static int Count { get { return _count; } }

        public const int WeatherStationId = 0;
        public const int MonthId = 1;
        public const int YearId = 2;
        public const int RainfallId = 3;
        public const int SunshineId = 4;
        public const int TempMeanMaxId = 5;
        public const int TempMeanMinId = 6;
        public const int AirFrostDaysId = 7;

        public double WeatherStation => int.Parse(_data[WeatherStationId].ToArray().Last().ToString());
         
        public int Month => GetMonthNumber(_data[MonthId]); 
        public int Year => int.Parse(_data[YearId]); 
        public double Rainfall => double.Parse(_data[RainfallId]); 
        public double Sunshine => double.Parse(_data[SunshineId]); 
        public double TempMeanMax => double.Parse(_data[TempMeanMaxId]); 
        public double TempMeanMin =>double.Parse(_data[TempMeanMinId]); 
        public int AirFrostDays => int.Parse(_data[AirFrostDaysId]); 

        public int GetMonthNumber(string month) {
            return Array.IndexOf(_months, month) + 1;
        }

        private string[] _data = new string[8];

        public WeatherDatum() {
            _count++;
        }

        public void SetData(int index, string value) {
            _data[index] = value;
        }
        
        public override string ToString()
        {
            return string.Format(
                "Year: {0}  \tMonth: {1} \tWeather Station: {2} \nTotal Hours of Rainfall: {3} \tTotal Hours of Sunshine: {4} \nMean Max Temperature: {5} \tMean Min Temperature: {6} \nDays of Air Frost: {7} \n", Year, _months[Month-1], WeatherStation, Rainfall, Sunshine, TempMeanMax, TempMeanMin, AirFrostDays);
        }
    }
}