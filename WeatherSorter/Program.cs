﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace WeatherSorter {
    class Program {
        static void Main(string[] args) {
            var dataManager = new DataManager();
            dataManager.LoadData();
            var weatherData = dataManager.WeatherData;

            var display = new Display();

            bool selection = false;

            Console.WriteLine("This is a data program for the Weather Stations Lerwick and Ross on Wye.");
            Console.WriteLine("Would you like to: \n[A]Search for a Year \n[B]Search for a Month \n[C]View all entries for a Weather Station \n[D]View all entries");
            do{
                Console.WriteLine("Enter your choice code");
                var menuSelection = Convert.ToChar(Console.ReadLine());
                switch (menuSelection) {
                    case 'A': //Search for Year
                        selection = true;
                        Console.WriteLine("Enter the year you wish to search for");
                        int searchYear = Convert.ToInt32((Console.ReadLine()));

                        display.DisplayManager(weatherData.Where(p => p.Year == searchYear).ToList());
                        break;

                    case 'B': //Search for Month
                        selection = true;
                        Console.WriteLine("Enter the month you wish to search for");
                        int searchMonth = Convert.ToInt32(Console.ReadLine());

                        display.DisplayManager(weatherData.Where(p => p.Month == searchMonth).ToList());
                        break;

                    case 'C': //Search for WS
                        selection = true;
                        Console.WriteLine("Enter the WeatherStation you wish to view");
                        int searchWS = Convert.ToInt32(Console.ReadLine()); 

                        display.DisplayManager(weatherData.Where(p => p.WeatherStation == searchWS).ToList());

                        break;
                    case 'D': //Display all
                        selection = true;
                        display.DisplayManager(weatherData);



                        break;
                    default:
                        Console.WriteLine("Incorrect Option");
                        break;
                }
            } while (selection == false);
        }

        private static int BinarySearch(int target, int[] array) { //useless as of yet
            int midpointIndex;
            var startIndex = 0;
            var endIndex = array.Length - 1;

            while (startIndex <= endIndex) {

                midpointIndex = (startIndex + endIndex) / 2;

                if (target == array[midpointIndex])
                    return midpointIndex;

                else if (target > array[midpointIndex])
                    startIndex = midpointIndex + 1;

                else
                    endIndex = midpointIndex - 1;

            }
            // Throw an exception is it doesn't work
            throw new Exception("Is your array even sorted?");
        }

        private static WeatherDatum[] InsertionSortByDate(WeatherDatum[] array) { //useless as of yet

            for (int i = 0; i < array.Length - 1; i++) {
                var j = i + 1;

                while (j > 0) {
                    if (array[j - 1].Year > array[j].Year) {

                        var temp = array[j - 1];
                        array[j - 1] = array[j];
                        array[j] = temp;
                    }

                    j--;
                }
            }
            return array;
        }
    }
}
