﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherSorter
{
    public class DataManager
    {

        private List<WeatherDatum> _weatherData = new List<WeatherDatum>();
        public List<WeatherDatum> WeatherData { get { return _weatherData; } }

        private IEnumerable<string> _dataPaths;

        public const string Directory = "../../Data/";
        public readonly string[] WeatherStations = new string[] { "WS1", "WS2" };
        public const string Month = "Month.txt";
        public const string Year = "Year.txt";
        public const string Rain = "_Rain.txt";
        public const string AirFrost = "_AF.txt";
        public const string Sun = "_Sun.txt";
        public const string TempMeanMax = "_TMax.txt";
        public const string TempMeanMin = "_TMin.txt";

        public DataManager()
        {
            _dataPaths = System.IO.Directory.GetFiles(Directory);
        }

        public void LoadData()
        {
            var monthData = GetDataFromFilename(_dataPaths, Month);
            var yearData = GetDataFromFilename(_dataPaths, Year);
            var metadataNames = new[] { Year, Month };

            SetMetadata(monthData, yearData);
            SetData(_dataPaths, metadataNames);
        }

        private void SetData(IEnumerable<string> dataPaths, string[] excludeArray)
        {
            var dataDataPaths = dataPaths.Where(p => !excludeArray.Contains(p));

            foreach (var weatherStation in WeatherStations)
            {
                var paths = dataDataPaths.Where(p => p.Split('/').Last().StartsWith(weatherStation)).ToList();
                var weatherData = WeatherData.Where(d => d.WeatherStation == int.Parse(weatherStation.ToArray().Last().ToString())).ToList();

                var rainData = GetDataFromFilename(paths, weatherStation + Rain);
                var sunData = GetDataFromFilename(paths, weatherStation + Sun);
                var airFrostData = GetDataFromFilename(paths, weatherStation + AirFrost);
                var tempMeanMax = GetDataFromFilename(paths, weatherStation + TempMeanMax);
                var tempMeanMin = GetDataFromFilename(paths, weatherStation + TempMeanMin);

                var data = new[] { rainData, sunData, airFrostData, tempMeanMax, tempMeanMin };
                var ids = new[] {
                    WeatherDatum.RainfallId,
                    WeatherDatum.SunshineId,
                    WeatherDatum.AirFrostDaysId,
                    WeatherDatum.TempMeanMaxId,
                    WeatherDatum.TempMeanMinId
                };

                for (int i = 0; i < weatherData.Count(); i++)
                {
                    for (int j = 0; j < data.Length; j++)
                    {
                        weatherData[i].SetData(ids[j], data[j][i]);
                    }
                }
            }
        }

        private void SetMetadata(string[] monthData, string[] yearData)
        {
            for (int i = 0; i < monthData.Count(); i++)
            {
                for (int j = 0; j < WeatherStations.Count(); j++)
                {
                    var weatherDatum = new WeatherDatum();
                    weatherDatum.SetData(WeatherDatum.WeatherStationId, WeatherStations[j]);
                    weatherDatum.SetData(WeatherDatum.MonthId, monthData[i]);
                    weatherDatum.SetData(WeatherDatum.YearId, yearData[i]);

                    _weatherData.Add(weatherDatum);
                }
            }
        }

        private string[] GetDataFromFilename(IEnumerable<string> dataPaths, string filename)
        {
            return System.IO.File.ReadAllLines(dataPaths.Where(p => p.Split('/').Last() == filename).First());
        }
    }
}

