﻿namespace WeatherSorter {
    public class WeatherDatum {
        private static int _count = 0;
        public static int Count { get { return _count; } }

        public const int WeatherStationId = 0;
        public const int MonthId = 1;
        public const int YearId = 2;
        public const int RainfallId = 3;
        public const int SunshineId = 4;
        public const int TempMeanMaxId = 5;
        public const int TempMeanMinId = 6;
        public const int AirFrostDaysId = 7;

        public int WeatherStation { get { return _data[0]; } }
        public string Month { get { return _data[1]; } }
        public int Year { get { return int.Parse(_data[2]); } }
        public double Rainfall { get { return double.Parse(_data[3]); } }
        public double Sunshine { get { return double.Parse(_data[4]); } }
        public double TempMeanMax { get { return double.Parse(_data[5]); } }
        public double TempMeanMin { get { return double.Parse(_data[6]); } }
        public int AirFrostDays { get { return int.Parse(_data[7]); } }

        private string[] _data = new string[8];

        public WeatherDatum() {
            _count++;
        }

        public void SetData(int index, string value) {
            _data[index] = value;
        }

        public override string ToString() {
            return string.Format(
                "Year: {0}  \tMonth: {1} \tWeather Station: {2} \nTotal Hours of Rainfall: {3} \tTotal Hours of Sunshine: {4} \nMean Max Temperature: {5} \tMean Min Temperature: {6} \nDays of Air Frost: {7}",Year, Month, WeatherStation, Rainfall,Sunshine,TempMeanMax,TempMeanMin,AirFrostDays);
       }
    }
}